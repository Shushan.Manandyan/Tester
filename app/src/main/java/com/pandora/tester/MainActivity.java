package com.pandora.tester;

import android.os.Bundle;
import android.os.Debug;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final ImageView ninePatchImage = findViewById(R.id.nine_patch);
        final ImageView simpledrawableImage = findViewById(R.id.simple_drawable);
        final TextView infoView = findViewById(R.id.info);

        findViewById(R.id.test_nine).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long nineTime = System.currentTimeMillis();
                for (int i = 0; i < 20000; i++) {
                    ninePatchImage.setImageResource(R.drawable.shado);
                }
                nineTime -= System.currentTimeMillis();

                infoView.setText("NinePatch - " + nineTime
                        + "\n VM Heap Size - " + Runtime.getRuntime().totalMemory()
                        + "\n Allocated VM Memory - " +(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())
                        + "\n VM Heap Size Limit - " +Runtime.getRuntime().maxMemory()
                        + "\n Native Allocated Memory - " + Debug.getNativeHeapAllocatedSize()
                );
            }
        });

        findViewById(R.id.test_simple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long simpleTime = System.currentTimeMillis();
                for (int i = 0; i < 20000; i++) {
                    simpledrawableImage.setImageResource(R.drawable.samsung_light_key_background);
                }
                simpleTime -= System.currentTimeMillis();
                infoView.setText("Simple - " + simpleTime
                        + "\n VM Heap Size - " + Runtime.getRuntime().totalMemory()
                        + "\n Allocated VM Memory - " +(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())
                        + "\n VM Heap Size Limit - " +Runtime.getRuntime().maxMemory()
                        + "\n Native Allocated Memory - " + Debug.getNativeHeapAllocatedSize()
                );
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
